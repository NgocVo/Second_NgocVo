// tasks.js
// This script manages a to-do list.

// Need a global variable:
var tasks = []; 

// Function called when the form is submitted.
// Function adds a task to the global array.

function addTask(e) {
    'use strict';
    
    // Get the task:
    var task = document.getElementById('task');

    // Reference to where the output goes:
    var output = document.getElementById('output');
    
    // For the output:
    var message = '';

    if (task.value) {
    
        // Add the item to the array:
        tasks.push(task.value);
        
        // Update the page:
        message = '<h2>To-Do</h2><ol>';
        for (var i = 0, count = tasks.length; i < count; i++) {
            message += '<li>' + tasks[i] + '</li>';
        }
        message += '</ol>';
        output.innerHTML = message;
        
    } // End of task.value IF.
    task.value='';
    
    // Return false to prevent submission:
    return false;
    
} // End of addTask() function.

//function remove duplicates

function removeDuplicates(e){
   
    e.preventDefault();
    var alt = [];
    for(var i = 0; i < tasks.length; i++){
        if(alt.indexOf(tasks[i]) == -1){
        alt.push(tasks[i]);
        }
    }
    tasks=alt;
     var output = document.getElementById('output');
     var message='';
     for (var i = 0, count = tasks.length; i < count; i++) {
            message += '<li>' + tasks[i] + '</li>';
        }
        message += '</ol>';
        output.innerHTML = message;
}
// Initial setup:

//document.getElementById('theForm').onsubmit = addTask;
var but = document.getElementById('button');
var subButton = document.getElementById('submit');
but.onclick = removeDuplicates;
subButton.onclick = addTask;
