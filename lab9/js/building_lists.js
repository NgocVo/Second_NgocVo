
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;
function createTable(){
    var $table=$('<table/>');
    $table.addClass('table table-striped');
    var $thread=$('<thead/>');
    $thread.append('<tr><th>Title</th><th>Year</th></tr>');
    $table.append($thread);
    var $tbody = $('<tbody/>');
    for(var i=0; i<books.length;i++){
        $tbody.append('<tr><td>'+books[i].title+'</td><td>'+books[i].year+'</td></tr>');
    }
    $table.append($tbody);
    $('#table').append($table);
}

/*var list = document.createElement('ul');
for (var i=0; i < books.length; i++) {
	console.log(books[i].title);
	var item = document.createElement('li');
	item.innerHTML = books[i].title+"  "+ books[i].year;
	list.appendChild(item);
}
document.body.appendChild(list);*/
