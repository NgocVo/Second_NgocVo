var fakeSlowNetwork;

// you can ignore this immediately-executing function
// it is used to simulate a slow network to show you how AJAX and Promises work
(function() {
  var lsKey = 'fake-slow-network';
  var networkFakeDiv = document.querySelector('.network-fake');
  var checkbox = networkFakeDiv.querySelector('input');

  fakeSlowNetwork = Number(localStorage.getItem(lsKey)) || 0;

  networkFakeDiv.style.display = 'block';
  checkbox.checked = !!fakeSlowNetwork;
    console.log(checkbox.checked);

  checkbox.addEventListener('change', function() {
    localStorage.setItem(lsKey, Number(checkbox.checked));
    location.reload();
  });
}());


// here is where the "real" example code starts
// it includes a few lines to "slow down" the AJAX calls
//   to simulate a real network scenario which you can ignore for now
//   but which will be relevant when we construct ASYNC callbacks next week

// ******* 
// AJAX STUFF
// *******
function getSync(url) {

  var req = new XMLHttpRequest();
  req.open('get', url, false);
  req.send();

  // pause here to simulate slow network - IGNORE
  var startTime = Date.now();
  var waitTime = 3000 * Math.random() * fakeSlowNetwork;
 
  while (waitTime > Date.now() - startTime);
   
  // now continue
  if (req.status == 200) {
    return req.response;
  }
  else {
    throw Error(req.statusText || "Request failed");
  }
}

function getJsonSync(url) {
  var e =JSON.parse(getSync(url));
  
    return e;
}
function getDictum(word) {

  var req = new XMLHttpRequest();
  req.open('get', 'http://api.wordnik.com:80/v4/word.json/'+word+'/definitions?limit=200&includeRelated=true&useCanonical=false&includeTags=false&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5', false);
  req.send();

  // pause here to simulate slow network - IGNOREj

 
   
  // now continue
  if (req.status == 200) {
      var e= req.response;
      
    return e;
  }
  else {
    throw Error(req.statusText || "Request failed");
  }
}

function addWord(word){
    var defi=getDictum(word);
    console.log(defi);
   
      
    defi=JSON.parse(defi);
    if(defi.length==0){
        
        var pa= document.createElement("p");
  
        pa.innerHTML="<h4>"+word+": unknown </h4>";
        document.getElementById("translate").appendChild(pa);
    }
    else{
    var pa= document.createElement("p");
  
    pa.innerHTML="<h4>"+word+": "+defi[0]["attributionText"]+"/<h4>";
    document.getElementById("translate").appendChild(pa);
    }
}

function getJsonSync(url) {
  var e =JSON.parse(getSync(url));
 
    return e;
}

// ******* 
// DOM STUFF
// *******
var storyDiv = document.querySelector('.story');

function addHtmlToPage(content) {
  var div = document.createElement('div');
  div.innerHTML = content;
  storyDiv.appendChild(div);
}

function addTextToPage(content) {
  var p = document.createElement('p');
  p.textContent = content;
  storyDiv.appendChild(p);
}